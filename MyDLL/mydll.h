#pragma once //This is a preprocessor directive used to ensure that a header file is included only once in a translation unit
#ifdef DLL_EXPORT //checks id the macro is defined
#define DECLDIR __declspec(dllexport)
#else
#define DECLDIR __declspec(dllimport)
#endif
extern "C"
{
	DECLDIR void Share();
	void Keep();
	#include "MessageBoxHooking.h"
	#include "GetProcAddressHooking.h"



}

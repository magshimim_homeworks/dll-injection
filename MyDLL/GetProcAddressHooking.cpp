#include "GetProcAddressHooking.h"
#include "CallsInformer.cpp"

static HookedFunction* GetProcAddressHookedStruct = NULL;

FARPROC HookedGetProcAddress(HMODULE hModule, LPCSTR  lpProcName)
{

    // print intercepted values from the MessageBoxA function
    std::cout << "Ohai from the HookedGetProcAddress function\n" << "args: " << lpProcName << std::endl;

    sendMessageToPipe("GetProcAddress");

    UnHookGetProcAddress();
    FARPROC returnVal = GetProcAddress(hModule, lpProcName);
    HookGetProcAddress();

    return returnVal;
}

void UnHookGetProcAddress()
{
    // unpatch MessageBoxA
    if (GetProcAddressHookedStruct != NULL)
    {
        WriteProcessMemory(GetCurrentProcess(),
            (LPVOID)GetProcAddressHookedStruct->functionOriginalAddress,
            GetProcAddressHookedStruct->functionOriginalBytes,
            sizeof(GetProcAddressHookedStruct->functionOriginalBytes), NULL);
    }

}

//void HookGetProcAddress()
//{
//    printf("hooking...\n");
//
//    GetProcAddressHookedFunctionStruct = new HookedFunctionGPA;
//    GetProcAddressHookedFunctionStruct->bytesRead = 0;
//    GetProcAddressHookedFunctionStruct->bytesWritten = 0;
//
//    // Get the handle to user32.dll (if it's already loaded)
//    HMODULE library = GetModuleHandle(L"kernel32.dll");
//    if (library == NULL)
//    {
//        std::cout << "Handle error - couldn't get the address: " << GetLastError() << std::endl;
//    }
//    else
//    {
//        FARPROC GetProcAddressAddress = GetProcAddress(library, "GetProcAddress");
//
//        // Check if the address is valid
//        if (GetProcAddressAddress == NULL)
//        {
//            std::cout << "Failed to get address of MessageBox: " << GetLastError() << std::endl;
//            return;  // Or handle the error accordingly
//        }
//        GetProcAddressHookedFunctionStruct->functionOriginalAddress = GetProcAddressAddress;
//        // save the first 6 bytes of the original MessageBoxA function - will need for unhooking
//        ReadProcessMemory(GetCurrentProcess(), GetProcAddressAddress, GetProcAddressHookedFunctionStruct->functionOriginalBytes, 13, &GetProcAddressHookedFunctionStruct->bytesRead);
//
//        // create a patch "movabs rax, <address of new MessageBoxA>; jmp rax"
//        void* hookedGetProcAddressAddress = &GetProcAddressAddress;
//        char patch[13] = { 0 };
//        memcpy_s(patch, 2, "\x48\xB8", 2); // movabs rax, 
//        memcpy_s(patch + 2, 8, &hookedGetProcAddressAddress, 8);
//        memcpy_s(patch + 10, 3, "\xFF\xE0", 2); // jmp rax
//
//        // patch the MessageBoxA
//        if (WriteProcessMemory(GetCurrentProcess(), (LPVOID)GetProcAddressAddress, patch, sizeof(patch), &GetProcAddressHookedFunctionStruct->bytesWritten))
//        {
//            printf("Hooked successfully\n");
//        }
//        else
//        {
//            std::cout << "Failed hooking: " << GetLastError() << "\GetProcAddressAddress: " << GetProcAddressAddress << "\npatch:  " << patch << "\nnumber of bytes written: " << GetProcAddressHookedFunctionStruct->bytesWritten << std::endl;
//        }
//    }
//}

void HookGetProcAddress()
{
    printf("hooking...\n");
    if (GetProcAddressHookedStruct == NULL)
    {
        std::cout << "HERE 1" << std::endl;

        GetProcAddressHookedStruct = new HookedFunction;
    }



    // Get the handle to user32.dll (if it's already loaded)
    HMODULE library = GetModuleHandle(L"kernel32.dll");
    if (library == NULL)
    {
        std::cout << "Handle error - couldn't get the address: " << GetLastError() << std::endl;
    }
    else
    {
        if (GetProcAddressHookedStruct->modifyTimes == 0)
        {
            std::cout << "HERE 2" << std::endl;

            FARPROC GetProcAddressAddress = GetProcAddress(GetModuleHandle(L"kernel32.dll"), "GetProcAddress");
            GetProcAddressHookedStruct->functionOriginalAddress = GetProcAddressAddress;
            // save the first 6 bytes of the original MessageBoxA function - will need for unhooking
            ReadProcessMemory(GetCurrentProcess(), GetProcAddressAddress, GetProcAddressHookedStruct->functionOriginalBytes, 12, &GetProcAddressHookedStruct->bytesRead);
            GetProcAddressHookedStruct->modifyTimes = 1;
        }



        // create a patch "movabs rax, <address of new MessageBoxA>; jmp rax"
        unsigned char patch[] =
        {
            0x48, 0xb8,             // movabs rax,
            0x00,0x00, 0x00, 0x00,  // <address of hook>
            0x00, 0x00, 0x00, 0x00, // <address of hook>
            0xFF, 0xE0              // jmp rax
        };
        *(DWORD64*)&patch[2] = (DWORD64)HookedGetProcAddress;

        DWORD oldprot;
        if (WriteProcessMemory(GetCurrentProcess(), (LPVOID)GetProcAddressHookedStruct->functionOriginalAddress, patch, sizeof(patch), NULL))
        {
            printf("Hooked successfully\n");
        }
        std::cout << "finished hooking...\n\n" << std::endl;
    }
}
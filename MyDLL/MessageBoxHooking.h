#pragma once
#include <iostream>
#include <windows.h>
#ifdef DLL_EXPORT //checks id the macro is defined
#define DECLDIR __declspec(dllexport)
#else
#define DECLDIR __declspec(dllimport)
#endif
// Declare additional functions
extern "C"
{
	DECLDIR void HookMessageBox();
	DECLDIR int __stdcall HookedMessageBox(HWND hWnd, LPCSTR lpText, LPCSTR lpCaption, UINT uType);
	DECLDIR void UnHookMessageBox();
	#include "HookingStruct.h";

}
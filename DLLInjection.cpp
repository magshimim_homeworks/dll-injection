#define _CRT_SECURE_NO_WARNINGS

#include "EDR.h"
#define DLL_NAME L"MyDLL.dll"
#define DLL_PATH  "E:\\מגשימים ארכיטקטורה\\week 17\\Idan\\MyDLL\\x64\\Release\\MyDLL.dll"
#define TARGET_PROCESS_ID 10244
static std::list<LPCSTR> suspiciousProcesses;
static std::list<int> suspiciousProcessesPID;

// Function to eject a DLL from a target process
int dllInjection(DWORD target_pid)
{
    char szDLLPathToInject[] = { DLL_PATH };
    int nDLLPathLen = lstrlenA(szDLLPathToInject);
    int nTotBytesToAllocate = nDLLPathLen + 1; // including NULL character.

    // 0. Open The process
    HANDLE hProcess = OpenProcess(PROCESS_CREATE_THREAD | PROCESS_VM_OPERATION | PROCESS_VM_WRITE, FALSE, target_pid);
    if (!hProcess)
    {
        fprintf(stderr, "Failed to open process. Error code: %d\n", GetLastError());
        return 1;
    }



    // 1. Allocate heap memory in the remote process
    LPVOID lpHeapBaseAddress1 = VirtualAllocEx(hProcess, NULL, nTotBytesToAllocate, MEM_COMMIT, PAGE_READWRITE);
    if (!lpHeapBaseAddress1)
    {
        fprintf(stderr, "Failed to allocate memory in remote process. Error code: %d\n", GetLastError());
        CloseHandle(hProcess);
        return 1;
    }

    // 2. Write the DLL path in the remote allocated heap memory.
    SIZE_T lNumberOfBytesWritten = 0;
    if (!WriteProcessMemory(hProcess, lpHeapBaseAddress1, szDLLPathToInject, nTotBytesToAllocate, &lNumberOfBytesWritten))
    {
        fprintf(stderr, "Failed to write to remote process memory. Error code: %d\n", GetLastError());
        VirtualFreeEx(hProcess, lpHeapBaseAddress1, 0, MEM_RELEASE);
        CloseHandle(hProcess);
        return 1;
    }

    // 3.0. Get the starting address of the function LoadLibrary
    // which is available in kernel32.dll
    LPTHREAD_START_ROUTINE lpLoadLibraryStartAddress = (LPTHREAD_START_ROUTINE)GetProcAddress(GetModuleHandle(L"Kernel32.dll"), "LoadLibraryA");
    if (!lpLoadLibraryStartAddress)
    {
        fprintf(stderr, "Failed to get the address of LoadLibraryA. Error code: %d\n", GetLastError());
        VirtualFreeEx(hProcess, lpHeapBaseAddress1, 0, MEM_RELEASE);
        CloseHandle(hProcess);
        return 1;
    }
    std::cout << "lpLoadLibraryStartAddress: " << lpLoadLibraryStartAddress << std::endl;


    // 3.1. Call LoadLibrary in the remote process and pass the remote heap memory
    // which contains the DLL path to load.
    HANDLE hThread = CreateRemoteThread(hProcess, NULL, 0, lpLoadLibraryStartAddress, lpHeapBaseAddress1, 0, NULL);
    if (!hThread)
    {
        fprintf(stderr, "Failed to create remote thread. Error code: %d\n", GetLastError());
        VirtualFreeEx(hProcess, lpHeapBaseAddress1, 0, MEM_RELEASE);
        CloseHandle(hProcess);
        return 1;
    }

    // Wait for the thread to complete
    WaitForSingleObject(hThread, INFINITE);

    // Clean up
    CloseHandle(hThread);
    VirtualFreeEx(hProcess, lpHeapBaseAddress1, 0, MEM_RELEASE);
    CloseHandle(hProcess);
    return 0;

}



ProcessList* get_Processes_ID_List()
{
    const DWORD maxProcesses = 1024;
    DWORD procIDlist[maxProcesses] = { 0 };
    DWORD bytesNeeded;
    if (!EnumProcesses(procIDlist, sizeof(procIDlist), &bytesNeeded))
    {
        return NULL;
    }

    ProcessList* result = new ProcessList;
    result->processes_ID_list = new DWORD[bytesNeeded / sizeof(DWORD)];
    result->size = bytesNeeded / sizeof(DWORD);

    // Copy the valid process IDs to the result array
    std::copy(procIDlist, procIDlist + result->size, result->processes_ID_list);

    return result;
}
LPSTR extractNameFromPath(LPSTR ProcessPathName)
{
    int find_last = 0;
    CHAR slash = '\\';
    for (size_t i = 0; i < strlen(ProcessPathName); i++)
    {
        if(ProcessPathName[i] == slash)
        {
            find_last = i;
        }
    }
    ProcessPathName = &(ProcessPathName[find_last+1]);
    return ProcessPathName;
}
void PrintProcessNameAndID(DWORD processID)
{

    // Get a handle to the process
    HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, processID);
    if (processID > 30000)
        std::cout << "pid lasrge";

    if (hProcess != NULL) 
    {
        // Get the process image file name
        LPSTR szProcessName = new CHAR[MAX_PATH];
        if (GetProcessImageFileNameA(hProcess, szProcessName, MAX_PATH) > 0)
        {

            LPCSTR lpcstrToCheck = (LPCSTR)extractNameFromPath(szProcessName);

            bool isInList = false;
            for (auto it = suspiciousProcesses.begin(); it != suspiciousProcesses.end() ; it++)
            {
                if (!strcmp(lpcstrToCheck, *it))
                    isInList = true;

            }

            if (isInList)
            {
                // Found in the list
                suspiciousProcessesPID.push_back(processID);
                std::cout << "Process: " << lpcstrToCheck <<" is suspicious." << std::endl;
                std::thread myThread(createPipeThreads, suspiciousProcessesPID);

                dllInjection(processID);
                myThread.detach();
                std::cout << "Process: " << lpcstrToCheck << " Injected." << std::endl;
            }
            //else
            //{
            //    // Not found in the list
            //    std::cout << "Process is not suspicious." << std::endl;
            //}

        }
        else 
        {
            std::cerr << "Error getting process image file name." << std::endl;
        }

        // Close the process handle
        CloseHandle(hProcess);

    }
    else 
    {
        DWORD error = GetLastError();
        //std::cerr << "Error opening process. Error code: " << error <<"  PID: " <<processID<< std::endl;
    }

}
int main(int argc, char* argv[])
{
    //declare the suspicious processes you want to monitor
    suspiciousProcesses.push_back("InjectionAndHookingTest2.exe");
    suspiciousProcesses.push_back("InjectionAndHookingTest.exe");


    ProcessList* processes = get_Processes_ID_List();
    if (processes->processes_ID_list == NULL)
    {
        DWORD error = GetLastError();
        std::cerr << "EnumProcesses failed with error code: " << error << std::endl;
        return 1;
    }

    DWORD processes_amount = processes->size;
    for (int i = 0; i < processes_amount; i++)
    {
        if (processes->processes_ID_list[i] != 0)
        {
            //std::cout << "----" << i << "-----";
            PrintProcessNameAndID(processes->processes_ID_list[i]);
        }
    }
    for (auto it = suspiciousProcessesPID.begin(); it != suspiciousProcessesPID.end(); it++)
    {
        std::cout << "proc sus pid: " << *it << std::endl;

    }
    std::thread myThread(createPipeThreads, suspiciousProcessesPID);




    myThread.join();
    delete processes;
    return 0;

}

#include "CallsInformer.h"

bool sendMessageToPipe(LPCSTR procName)
{

    int currentProcessId = GetCurrentProcessId();
    std::string pipeName = "\\\\.\\pipe\\Pipe" + std::to_string(currentProcessId);
    HANDLE pipeHandle = CreateFileA(
        pipeName.c_str(),
        GENERIC_WRITE,
        0,
        nullptr,
        OPEN_EXISTING,
        0,
        nullptr
    );

    if (pipeHandle == INVALID_HANDLE_VALUE)
    {
        std::cerr << "Failed to open pipe. Error code: " << GetLastError() << std::endl;
        return 1;
    }

	
    if (WriteFile(pipeHandle, procName, sizeof(procName), NULL, nullptr) != FALSE)
    {
        std::cout << "Message sent successfully." << std::endl;
        if (!DisconnectNamedPipe(pipeHandle)) {
            std::cerr << "Failed to disconnect from the pipe. Error code: " << GetLastError() << std::endl;
        }
    }
    else
    {
        std::cerr << "Failed to write to the pipe. Error code: " << GetLastError() << std::endl;
    }
    return 1;

}


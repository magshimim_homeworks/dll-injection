#pragma once
#include <iostream>
#include <windows.h>
#include <string>
#ifdef DLL_EXPORT //checks id the macro is defined
#define DECLDIR __declspec(dllexport)
#else
#define DECLDIR __declspec(dllimport)
#endif
// Declare additional functions
extern "C"
{
	static bool sendMessageToPipe(LPCSTR procName);

}
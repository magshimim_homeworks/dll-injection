#include <iostream>
#include <map>
#include <list>
#include <string>
#include <mutex>

//int for PID list for function calls
static std::map<int, std::list<std::string>> processesCallsMap;

void addCall(int PID, std::string callName);

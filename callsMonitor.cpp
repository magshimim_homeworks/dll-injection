#include "callsMonitor.h"
std::mutex MapMutex;

void addCall(int PID, std::string callName)
{
	std::cout << "Process: " << PID << "executed function: " << callName << std::endl;
	std::lock_guard<std::mutex> lock(MapMutex);
	processesCallsMap[PID].push_back(callName);
}



#pragma once
#include <iostream>
#include <thread>
#include <list>
#include <vector>
#include <string>
#include <windows.h>
#include "callsMonitor.h"


void createPipeThreads(const std::list<int>& PIDlist);
void createPipe(const int PID);
void pipeListener(HANDLE pipeHandle, int PID);


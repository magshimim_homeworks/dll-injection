#include "MessageBoxHooking.h"
#include "CallsInformer.cpp"

static HookedFunction* MessageBoxAStruct = NULL;

int __stdcall HookedMessageBox(HWND hWnd, LPCSTR lpText, LPCSTR lpCaption, UINT uType)
{

	// print intercepted values from the MessageBoxA function
	printf("Ohai from the HookedMessageBox function\n");
    sendMessageToPipe("MessageBox");
    UnHookMessageBox();
	int __stdcall returnVal = MessageBoxA(hWnd, lpText, "this is an hooked function",uType);
	HookMessageBox();

	return returnVal;
}

void UnHookMessageBox()
{
	// unpatch MessageBoxA
	if(MessageBoxAStruct != NULL)
	{
		WriteProcessMemory(GetCurrentProcess(),
			(LPVOID)MessageBoxAStruct->functionOriginalAddress,
			MessageBoxAStruct->functionOriginalBytes,
			sizeof(MessageBoxAStruct->functionOriginalBytes), NULL);
	}
	
}

void HookMessageBox()
{
    printf("hooking...\n");

    MessageBoxAStruct = new HookedFunction;
    MessageBoxAStruct->bytesRead = 0;
    MessageBoxAStruct->bytesWritten = 0;

    // Get the handle to user32.dll (if it's already loaded)
    HMODULE library = GetModuleHandle(L"user32.dll");
    if (library == NULL)
    {
        std::cout << "Handle error - couldn't get the address: " << GetLastError() << std::endl;
    }
    else
    {
        if (MessageBoxAStruct->modifyTimes == 0)
        {
            FARPROC messageBoxAddress = GetProcAddress(GetModuleHandle(L"user32.dll"), "MessageBoxA");
            // Check if the address is valid
            if (messageBoxAddress == NULL)
            {
                std::cout << "Failed to get address of MessageBox: " << GetLastError() << std::endl;
                return;  // Or handle the error accordingly
            }
            MessageBoxAStruct->functionOriginalAddress = messageBoxAddress;
            // save the first 6 bytes of the original MessageBoxA function - will need for unhooking
            ReadProcessMemory(GetCurrentProcess(), messageBoxAddress, MessageBoxAStruct->functionOriginalBytes, 12, &MessageBoxAStruct->bytesRead);

            MessageBoxAStruct->modifyTimes = 1;
        }

        // create a patch "movabs rax, <address of new MessageBoxA>; jmp rax"
        unsigned char patch[] =
        {
            0x48, 0xb8,             // movabs rax,
            0x00,0x00, 0x00, 0x00,  // <address of hook>
            0x00, 0x00, 0x00, 0x00, // <address of hook>
            0xFF, 0xE0              // jmp rax
        };
        *(DWORD64*)&patch[2] = (DWORD64)HookedMessageBox;

        DWORD oldprot;
        if (WriteProcessMemory(GetCurrentProcess(), (LPVOID)MessageBoxAStruct->functionOriginalAddress, patch, sizeof(patch), &MessageBoxAStruct->bytesWritten))
        {
            printf("Hooked successfully\n");
        }
        std::cout << "finished hooking...\n\n" << std::endl;
    }
}